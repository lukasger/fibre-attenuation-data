# Fibre Attenuation Data
The data is grouped in two parts: Power measurements obtained with a PIN diode
and spectra obtained with a spectrometer.

## PIN Diode
Grouped in a directory for each fibre type with ascii files per fibre sample. 
The first column denotes the distance from the closest point and the second
column denotes the power at the PIN diode. This can be seen as arbitrary since
coupling to the PIN diode and the LED may vary from sample to sample. 

## Spectra
Each file corresponds to a single spectrum. For the distance measurements,
dark spectra are also recorded after each measurement in order to be subtracted
from the spectra with light. The distances are denoted in a text file
"distances.txt" in each directory.
